var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Выводим список студентов

console.log("Список студентов:");
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log("Студент %s набрал %d баллов.", studentsAndPoints[i], studentsAndPoints[i+1]);
}

console.log(" ");

//Ищем студента с наибольшим количеством баллов

var maxIndex=-1, max;

for (var i = 1, jmax = studentsAndPoints.length; i < jmax; i += 2) {
  if (maxIndex < 0 || studentsAndPoints[i] > max){
  	maxIndex = i;
    max = studentsAndPoints[i];
  }
}

console.log("Студент набравший максимальный балл:");
console.log("Студент %s имеет максимальный балл %d", studentsAndPoints[maxIndex-1], max);

console.log(" ");

//Добавляем новых студентов

studentsAndPoints.push('Николай Фролов', 0, "Олег Боровой", 0);
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log("Студент %s набрал %d баллов.", studentsAndPoints[i], studentsAndPoints[i+1]);
}
console.log(" ");

/*Добавляем баллы студентам
  Добавляем без цикла*/

var antonIndex = studentsAndPoints.indexOf("Антон Павлович");
console.log("У студента %s было %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);
studentsAndPoints[antonIndex + 1] += 10;
console.log("У студента %s стало %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);

var nikolaiIndex = studentsAndPoints.indexOf("Николай Фролов");
console.log("У студента %s было %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);
studentsAndPoints[nikolaiIndex + 1] += 10;
console.log("У студента %s стало %d баллов", studentsAndPoints[i], studentsAndPoints[i+1]);

console.log(" ");

//Выводим студентов не набравших баллы

console.log("Студенты не набравшие баллы:")
for (var i = 0, imax = studentsAndPoints.length; i < imax; ++i) {
	if (studentsAndPoints[i] == 0){
  	console.log(studentsAndPoints[i-1]);
  }
}

console.log(" ");

/*Удаляем студентов не набравших баллы
  Исправил вариант*/

for (var i = studentsAndPoints.length; i > 0; --i) {
	if (studentsAndPoints[i] === 0){
  	studentsAndPoints.splice(i-1, 2);
  }
}

console.log("Студенты с баллами:")
for (var i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	console.log("Студент %s набрал %d баллов.", studentsAndPoints[i], studentsAndPoints[i+1]);
}
